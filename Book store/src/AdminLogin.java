import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminLogin extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminLogin frame = new AdminLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 769, 458);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("book.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 798, 125);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Admin:");
		lblNewLabel_1.setFont(new Font("Adobe Garamond Pro Bold", Font.BOLD, 14));
		lblNewLabel_1.setBounds(273, 201, 46, 19);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblPassword = new JLabel("Password: ");
		lblPassword.setFont(new Font("Adobe Caslon Pro Bold", Font.BOLD, 14));
		lblPassword.setBounds(273, 242, 61, 19);
		contentPane.add(lblPassword);
		
		textField = new JTextField();
		textField.setBounds(365, 200, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(365, 241, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		Image img2 = new ImageIcon(this.getClass().getResource("login (1).png")).getImage();
		btnLogin.setIcon(new ImageIcon(img2));
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLogin.setBounds(327, 295, 124, 23);
		contentPane.add(btnLogin);
		
		JButton btnGoingToHome = new JButton("Going To Home Page");
		btnGoingToHome.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnGoingToHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainPage.main(new String[] {});
			}
		});
		Image img5 = new ImageIcon(this.getClass().getResource("house.png")).getImage();
		btnGoingToHome.setIcon(new ImageIcon(img5));
		btnGoingToHome.setBounds(0, 124, 210, 33);
		contentPane.add(btnGoingToHome);
		
		
	}
}
