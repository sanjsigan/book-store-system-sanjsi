import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class AdminWelcome extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminWelcome frame = new AdminWelcome();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminWelcome() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 862, 476);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("New label");
		Image img = new ImageIcon(this.getClass().getResource("welcome.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(-26, 0, 862, 179);
		contentPane.add(lblNewLabel);

		JButton btnNewButton_1 = new JButton("Delete Details");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DeleteDetails.main(new String[] {});
			}
		});
		Image img2 = new ImageIcon(this.getClass().getResource("delete.png")).getImage();
		btnNewButton_1.setIcon(new ImageIcon(img2));
		btnNewButton_1.setBounds(330, 190, 207, 193);
		contentPane.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("Update Details");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateDetails.main(new String[] {});
			}
		});
		btnNewButton_2.setForeground(Color.WHITE);
		Image img3 = new ImageIcon(this.getClass().getResource("update.png")).getImage();
		btnNewButton_2.setIcon(new ImageIcon(img3));
		btnNewButton_2.setBounds(580, 190, 192, 193);
		contentPane.add(btnNewButton_2);

		JButton btnNewButton = new JButton("View Details");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewDetails.main(new String[] {});
			}
		});
		Image img1 = new ImageIcon(this.getClass().getResource("download.png")).getImage();
		btnNewButton.setIcon(new ImageIcon(img1));
		btnNewButton.setBounds(99, 190, 207, 193);
		contentPane.add(btnNewButton);

		JLabel lblViewDetails = new JLabel("View Details");
		lblViewDetails.setForeground(Color.RED);
		lblViewDetails.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblViewDetails.setBounds(149, 392, 98, 14);
		contentPane.add(lblViewDetails);

		JLabel lblDeleteDetails = new JLabel("Delete Details");
		lblDeleteDetails.setForeground(Color.RED);
		lblDeleteDetails.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDeleteDetails.setBounds(383, 392, 105, 14);
		contentPane.add(lblDeleteDetails);

		JLabel lblUpdate = new JLabel("Update");
		lblUpdate.setForeground(Color.RED);
		lblUpdate.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUpdate.setBounds(647, 383, 87, 32);
		contentPane.add(lblUpdate);

	}

}
