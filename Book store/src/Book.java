import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class Book extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Book frame = new Book();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	public Book() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 830, 444);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		table = new JTable();
		table.setBounds(0, 76, 319, 199);
		contentPane.add(table);

		JButton btnViewBookDetails = new JButton("View Book Details");
		btnViewBookDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String sql = ("select * from bookstore.book");
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) table.getModel();
					tm.setRowCount(0);

					table.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnViewBookDetails.setBounds(97, 304, 115, 23);
		contentPane.add(btnViewBookDetails);

		JLabel lblNewLabel = new JLabel("Book Number");
		lblNewLabel.setBounds(0, 51, 70, 14);
		contentPane.add(lblNewLabel);

		JLabel label = new JLabel("Book Name");
		label.setBounds(123, 51, 70, 14);
		contentPane.add(label);

		JLabel label_1 = new JLabel("Price");
		label_1.setBounds(242, 51, 77, 14);
		contentPane.add(label_1);

		JLabel lblNewLabel_1 = new JLabel("Book Number:");
		lblNewLabel_1.setBounds(490, 107, 88, 14);
		contentPane.add(lblNewLabel_1);

		JLabel label_2 = new JLabel("Book Name:");
		label_2.setBounds(490, 151, 88, 14);
		contentPane.add(label_2);

		JLabel label_3 = new JLabel("User Name:");
		label_3.setBounds(490, 196, 88, 14);
		contentPane.add(label_3);

		JLabel label_4 = new JLabel("Price");
		label_4.setBounds(490, 240, 77, 14);
		contentPane.add(label_4);

		JLabel label_5 = new JLabel("Creditcard Number:");
		label_5.setBounds(490, 283, 109, 14);
		contentPane.add(label_5);

		textField = new JTextField();
		textField.setBounds(609, 104, 123, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(609, 148, 123, 20);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(609, 193, 123, 20);
		contentPane.add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(609, 237, 123, 20);
		contentPane.add(textField_3);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(609, 280, 123, 20);
		contentPane.add(textField_4);

		JButton btnBuyYourBook = new JButton("Buy Your Book");
		btnBuyYourBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String boono = textField.getText();
				String booname = textField_1.getText();
				String name = textField_2.getText();
				String pr = textField_3.getText();
				String credit = textField_4.getText();

				try {
					String sql = ("insert into bookstore.buybook  (BookNumber,BookName,UserName,Price,Creditcard) values('"
							+ boono + "','" + booname + "','" + name + "','" + pr + "','" + credit + "')");
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "BUY sucessfully...");
					UserLogin.main(new String[] {});
				} catch (Exception e1) {
					System.out.println(e1);
				}
			}
		});
		btnBuyYourBook.setBounds(588, 341, 115, 23);
		contentPane.add(btnBuyYourBook);
	}
}
