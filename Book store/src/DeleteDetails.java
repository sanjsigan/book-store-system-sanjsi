import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class DeleteDetails extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteDetails frame = new DeleteDetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;

	public DeleteDetails() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 374, 277);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDvdNumber = new JLabel("DVD Number: ");
		lblDvdNumber.setBounds(25, 78, 80, 14);
		contentPane.add(lblDvdNumber);

		textField = new JTextField();
		textField.setBounds(115, 78, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnDeletedDvd = new JButton("Deleted DVD");
		btnDeletedDvd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dvdNo = textField.getText();
				try {

					String sql = "delete from bookstore.dvd where DVDNo='" + dvdNo + "'";
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "Your Account Successfully Deleted..");
				} catch (Exception e1) {
					System.out.println(e1);

				}
			}
		});
		btnDeletedDvd.setBounds(210, 77, 138, 23);
		contentPane.add(btnDeletedDvd);

		JLabel label = new JLabel("Book Number: ");
		label.setBounds(25, 109, 86, 14);
		contentPane.add(label);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(115, 106, 86, 20);
		contentPane.add(textField_1);

		JButton button = new JButton("Deleted BOOK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String bookNo = textField_1.getText();
				try {

					String sql = "delete from bookstore.book where BookNumber='" + bookNo + "'";
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "Your Account Successfully Deleted..");
				} catch (Exception e1) {
					System.out.println(e1);

				}
			}
		});
		button.setBounds(210, 105, 138, 23);
		contentPane.add(button);

		JButton btnBackToAdmin = new JButton("Back To Admin Home");
		btnBackToAdmin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnBackToAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminWelcome.main(new String[] {});
			}
		});
		Image img = new ImageIcon(this.getClass().getResource("back.png")).getImage();
		btnBackToAdmin.setIcon(new ImageIcon(img));
		btnBackToAdmin.setBounds(16, 177, 256, 50);
		contentPane.add(btnBackToAdmin);
	}
}
