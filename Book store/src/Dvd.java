import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.xdevapi.Statement;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JTextField;

public class Dvd extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dvd frame = new Dvd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;
	private JLabel lblDvdnumber;
	private JLabel label;
	private JLabel label_1;
	private JButton btnNewButton;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JLabel lblBuyYourDvds;
	private JLabel label_2;
	private JTextField textField_4;

	public Dvd() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		JOptionPane.showMessageDialog(null, "Connected to Database");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 832, 434);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		table = new JTable();
		table.setBounds(0, 54, 293, 203);
		contentPane.add(table);

		lblDvdnumber = new JLabel("DvdNumber");
		lblDvdnumber.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDvdnumber.setBounds(0, 33, 95, 14);
		contentPane.add(lblDvdnumber);

		label = new JLabel("Name");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(116, 33, 75, 14);
		contentPane.add(label);

		label_1 = new JLabel("Price");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_1.setBounds(220, 34, 75, 14);
		contentPane.add(label_1);

		btnNewButton = new JButton("View DVD Details");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String sql = ("select * from bookstore.dvd");
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) table.getModel();
					tm.setRowCount(0);

					table.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnNewButton.setBackground(Color.LIGHT_GRAY);
		btnNewButton.setForeground(Color.RED);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		Image img = new ImageIcon(this.getClass().getResource("eye.png")).getImage();
		btnNewButton.setIcon(new ImageIcon(img));
		btnNewButton.setBounds(51, 272, 187, 30);
		contentPane.add(btnNewButton);

		lblNewLabel = new JLabel("DVD Number: ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(489, 111, 111, 16);
		contentPane.add(lblNewLabel);

		lblNewLabel_1 = new JLabel("DVD Name: ");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setBounds(489, 158, 95, 14);
		contentPane.add(lblNewLabel_1);

		lblNewLabel_2 = new JLabel("User Name: ");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_2.setBounds(489, 207, 95, 14);
		contentPane.add(lblNewLabel_2);

		lblNewLabel_3 = new JLabel("Creditcard Number: ");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_3.setBounds(488, 293, 141, 14);
		contentPane.add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBounds(629, 111, 111, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(628, 157, 112, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(629, 206, 112, 20);
		contentPane.add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(629, 253, 112, 20);
		contentPane.add(textField_3);

		JButton btnNewButton_1 = new JButton("Buy Your DVD");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dvno = textField.getText();
				String dvname = textField_1.getText();
				String name = textField_2.getText();
				String pr = textField_3.getText();
				String credit = textField_4.getText();

				try {
					String sql = ("insert into bookstore.buydvd  (DvdNumber,DvdName,UserName,Price,CreditCardno) values('"
							+ dvno + "','" + dvname + "','" + name + "','" + pr + "','" + credit + "')");
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "BUY sucessfully...");
					UserLogin.main(new String[] {});
				} catch (Exception e1) {
					System.out.println(e1);
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		Image img1 = new ImageIcon(this.getClass().getResource("buy.png")).getImage();
		btnNewButton_1.setIcon(new ImageIcon(img1));
		btnNewButton_1.setBounds(576, 354, 169, 30);
		contentPane.add(btnNewButton_1);

		lblBuyYourDvds = new JLabel("BUY YOUR DVDs");
		lblBuyYourDvds.setForeground(Color.BLUE);
		lblBuyYourDvds.setFont(new Font("Snap ITC", Font.BOLD, 17));
		lblBuyYourDvds.setBounds(522, 55, 251, 30);
		contentPane.add(lblBuyYourDvds);

		label_2 = new JLabel("Price: ");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_2.setBounds(489, 254, 95, 14);
		contentPane.add(label_2);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(628, 292, 112, 20);
		contentPane.add(textField_4);
	}
}
