import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class MainPage extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPage frame = new MainPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 776, 455);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("book.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 768, 128);
		contentPane.add(lblNewLabel);

		JButton btnNewButton = new JButton("Admin");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminLogin.main(new String[] {});
			}
		});
		Image img1 = new ImageIcon(this.getClass().getResource("admin.png")).getImage();
		btnNewButton.setIcon(new ImageIcon(img1));
		btnNewButton.setBounds(139, 149, 203, 209);
		contentPane.add(btnNewButton);
		JButton btnNewButton_1 = new JButton("User");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserLogin.main(new String[] {});
			}
		});
		Image img2 = new ImageIcon(this.getClass().getResource("user1.png")).getImage();
		btnNewButton_1.setIcon(new ImageIcon(img2));
		btnNewButton_1.setBounds(393, 149, 212, 209);
		contentPane.add(btnNewButton_1);

		JLabel lblAdminLoginPage = new JLabel("Admin Login Page");
		lblAdminLoginPage.setFont(new Font("Stencil Std", Font.BOLD, 14));
		lblAdminLoginPage.setBounds(169, 369, 162, 25);
		contentPane.add(lblAdminLoginPage);

		JLabel label = new JLabel("User Login Page");
		label.setFont(new Font("Stencil Std", Font.BOLD, 14));
		label.setBounds(429, 369, 162, 25);
		contentPane.add(label);

	}
}
