import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class UpdateDetails extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateDetails frame = new UpdateDetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;

	public UpdateDetails() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 772, 448);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEnterDvdNumber = new JLabel("Enter DVD Number:");
		lblEnterDvdNumber.setBounds(30, 104, 158, 22);
		contentPane.add(lblEnterDvdNumber);

		JLabel lblUpdateDvdName = new JLabel("Update DVD Name:");
		lblUpdateDvdName.setBounds(30, 148, 107, 16);
		contentPane.add(lblUpdateDvdName);

		JLabel lblNewLabel = new JLabel("Update DVD Price:");
		lblNewLabel.setBounds(30, 194, 107, 14);
		contentPane.add(lblNewLabel);

		textField = new JTextField();
		textField.setBounds(179, 104, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(179, 144, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(179, 190, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JButton btnUpdateDvd = new JButton("Update DVD");
		btnUpdateDvd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dvdno = textField.getText();
				String dvdname = textField_1.getText();
				String price = textField_2.getText();

				try {

					String sql = "update bookstore.dvd set DVDName='" + dvdname + "',Price='" + price + "'where DVDNo='"
							+ dvdno + "'";

					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);

					JOptionPane.showMessageDialog(null, "Successfully Updated..");
				} catch (Exception e1) {
					System.out.println(e1);

				}
			}
		});
		btnUpdateDvd.setBounds(81, 241, 107, 23);
		contentPane.add(btnUpdateDvd);

		JLabel lblUpdateDvdDetails = new JLabel("UPDATE DVD DETAILS");
		lblUpdateDvdDetails.setBounds(81, 64, 125, 14);
		contentPane.add(lblUpdateDvdDetails);

		JLabel lblEnterBookNumber = new JLabel("Enter BOOK Number:");
		lblEnterBookNumber.setBounds(431, 108, 107, 14);
		contentPane.add(lblEnterBookNumber);

		JLabel lblUpdateDvdName_1 = new JLabel("Update DVD Name:");
		lblUpdateDvdName_1.setBounds(431, 149, 107, 14);
		contentPane.add(lblUpdateDvdName_1);

		JLabel lblUpdateDvdPrice = new JLabel("Update DVD Price:");
		lblUpdateDvdPrice.setBounds(431, 194, 107, 14);
		contentPane.add(lblUpdateDvdPrice);

		textField_3 = new JTextField();
		textField_3.setBounds(581, 105, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(581, 148, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		textField_5 = new JTextField();
		textField_5.setBounds(581, 191, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);

		JButton btnUpdateBook = new JButton("Update BOOK");
		btnUpdateBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String bookno = textField_3.getText();
				String bookname = textField_4.getText();
				String price = textField_5.getText();

				try {

					String sql = "update bookstore.dvd set BookName='" + bookname + "',Price='" + price
							+ "'where BookNumber='" + bookno + "'";

					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);

					JOptionPane.showMessageDialog(null, "Successfully Updated..");
				} catch (Exception e1) {
					System.out.println(e1);

				}
			}
		});
		btnUpdateBook.setBounds(503, 241, 114, 23);
		contentPane.add(btnUpdateBook);

		JLabel lblUpdateBookDetails = new JLabel("UPDATE BOOK DETAILS");
		lblUpdateBookDetails.setBounds(490, 64, 147, 14);
		contentPane.add(lblUpdateBookDetails);

		JButton btnBackToAdmin = new JButton("Back To Admin Home");
		btnBackToAdmin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnBackToAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminWelcome.main(new String[] {});
			}
		});
		Image img = new ImageIcon(this.getClass().getResource("back.png")).getImage();
		btnBackToAdmin.setIcon(new ImageIcon(img));
		btnBackToAdmin.setBounds(10, 351, 255, 46);
		contentPane.add(btnBackToAdmin);
	}

}
