import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.sql.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.protocol.Resultset;
import com.mysql.cj.xdevapi.Statement;
import java.sql.PreparedStatement;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class UserLogin extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;
	private JButton button;
	private JLabel lblNewLabel_1;
	private JLabel lblLoginYourAccount;
	private JButton btnNewButton_1;
	private JButton btnDeleteYourAccount;
	private JTextField textField_2;
	private JLabel lblUserName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserLogin frame = new UserLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	// Connecting database for only this class
	Connection conn = null;

	public UserLogin() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		// Connection End

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 777, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// Creating labels
		JLabel lblNewLabel = new JLabel("New label");
		Image img = new ImageIcon(this.getClass().getResource("book.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 761, 126);
		contentPane.add(lblNewLabel);

		JLabel lblUserId = new JLabel("User ID:");
		lblUserId.setFont(new Font("Stencil Std", Font.BOLD, 13));
		lblUserId.setBounds(237, 188, 123, 14);
		contentPane.add(lblUserId);

		JLabel label = new JLabel("Password:");
		label.setFont(new Font("Stencil Std", Font.BOLD, 13));
		label.setBounds(237, 239, 97, 14);
		contentPane.add(label);
		// Create text field
		textField = new JTextField();
		textField.setBounds(353, 186, 111, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(352, 237, 112, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		// Create buttons
		btnNewButton = new JButton("Login");
		btnNewButton.setFont(new Font("Berlin Sans FB Demi", Font.BOLD, 13));

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Create variables for get text
				String name = textField.getText();
				String pass = textField_1.getText();

				try {
					// select statement for get name and password
					// and connect mysql
					String sql = "select * from bookstore.bookstore  where UserName='" + name + "' and CreatePassword='"
							+ pass + "'";
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					if (rs.next()) {
						JOptionPane.showMessageDialog(null, "Yess Successfully..");
						setVisible(false);
						// open next page
						UserWelcome vw = new UserWelcome();
						vw.setVisible(true);

					} else {
						JOptionPane.showMessageDialog(null, "Username or Passwor are wrong!!");
					}

				} catch (Exception e2) {
					System.out.println(e2);

				}

			}
		});
		// Add image in button
		Image img2 = new ImageIcon(this.getClass().getResource("login (1).png")).getImage();
		btnNewButton.setIcon(new ImageIcon(img2));
		btnNewButton.setBounds(219, 288, 106, 33);
		contentPane.add(btnNewButton);

		button = new JButton("SignUp");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserSignup.main(new String[] {});
			}
		});
		// Add image in button
		Image img3 = new ImageIcon(this.getClass().getResource("signup.png")).getImage();
		button.setIcon(new ImageIcon(img3));
		button.setFont(new Font("Berlin Sans FB Demi", Font.BOLD, 13));
		button.setBounds(355, 288, 111, 33);
		contentPane.add(button);
		// Add image in label
		lblNewLabel_1 = new JLabel("");
		Image img4 = new ImageIcon(this.getClass().getResource("password.png")).getImage();
		lblNewLabel_1.setIcon(new ImageIcon(img4));
		lblNewLabel_1.setBounds(496, 144, 235, 177);
		contentPane.add(lblNewLabel_1);

		// create login your account label
		lblLoginYourAccount = new JLabel("Login Your Account.....");
		lblLoginYourAccount.setForeground(new Color(138, 43, 226));
		lblLoginYourAccount.setFont(new Font("Snap ITC", Font.BOLD, 15));
		lblLoginYourAccount.setBounds(506, 305, 215, 20);
		contentPane.add(lblLoginYourAccount);

		// create one button
		btnNewButton_1 = new JButton("Going to Home Page");
		btnNewButton_1.setForeground(new Color(0, 0, 255));
		btnNewButton_1.setFont(new Font("Stencil Std", Font.BOLD, 13));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainPage.main(new String[] {});
			}
		});
		// Add image in button
		Image img5 = new ImageIcon(this.getClass().getResource("house.png")).getImage();
		btnNewButton_1.setIcon(new ImageIcon(img5));
		btnNewButton_1.setBounds(0, 125, 225, 41);
		contentPane.add(btnNewButton_1);

		// create one button
		btnDeleteYourAccount = new JButton("Delete Your Account");
		btnDeleteYourAccount.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnDeleteYourAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Delete statement
				String deluser = textField_2.getText();
				try {

					String sql = "delete from bookstore.bookstore where UserName='" + deluser + "'";
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "Your Account Successfully Deleted..");
				} catch (Exception e2) {
					System.out.println(e2);

				}

			}
		});
		btnDeleteYourAccount.setBounds(0, 268, 149, 23);
		contentPane.add(btnDeleteYourAccount);

		textField_2 = new JTextField();
		textField_2.setBounds(0, 237, 111, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		lblUserName = new JLabel("User Name");
		lblUserName.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUserName.setBounds(10, 212, 101, 14);
		contentPane.add(lblUserName);

	}
}
