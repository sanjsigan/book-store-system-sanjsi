import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.sql.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class UserSignup extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserSignup frame = new UserSignup();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;

	public UserSignup() throws SQLException {

		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		JOptionPane.showMessageDialog(null, "Connected to Database");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 769, 497);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("New label");
		Image img = new ImageIcon(this.getClass().getResource("book.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 751, 138);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Customer ID:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_1.setBounds(177, 184, 85, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("User Name:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_2.setBounds(177, 230, 85, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Gender:");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_3.setBounds(177, 272, 85, 14);
		contentPane.add(lblNewLabel_3);

		JLabel label = new JLabel("Address: ");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(177, 310, 85, 14);
		contentPane.add(label);

		JLabel label_1 = new JLabel("Mobile Number:");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_1.setBounds(177, 353, 109, 14);
		contentPane.add(label_1);

		JLabel label_2 = new JLabel("Create Your Password:");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_2.setBounds(177, 392, 159, 14);
		contentPane.add(label_2);

		textField = new JTextField();
		textField.setBounds(398, 182, 109, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(398, 228, 109, 20);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(398, 270, 109, 20);
		contentPane.add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(398, 308, 109, 20);
		contentPane.add(textField_3);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(398, 351, 109, 20);
		contentPane.add(textField_4);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(398, 390, 109, 20);
		contentPane.add(textField_5);

		JButton btnNewButton = new JButton("Sign Up");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String cusid = textField.getText();
				String name = textField_1.getText();
				String gender = textField_2.getText();
				String address = textField_3.getText();
				String mobile = textField_4.getText();
				String pass = textField_5.getText();

				try {
					String sql = ("insert into bookstore.bookstore  (CustomerId,UserName,Gender,Address,MobileNumber,CreatePassword) values('"
							+ cusid + "','" + name + "','" + gender + "','" + address + "','" + mobile + "','" + pass
							+ "')");
					Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "Sign up sucessfully...");
					UserLogin.main(new String[] {});
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		Image img2 = new ImageIcon(this.getClass().getResource("sign2.png")).getImage();
		btnNewButton.setIcon(new ImageIcon(img2));
		btnNewButton.setBounds(540, 417, 147, 30);
		contentPane.add(btnNewButton);

		JButton btnUpdateDetail = new JButton("Update Detail");
		btnUpdateDetail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateUser.main(new String[] {});
			}
		});
		btnUpdateDetail.setBounds(540, 373, 147, 23);
		contentPane.add(btnUpdateDetail);
	}
}
