import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.xdevapi.Statement;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class UserWelcome extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserWelcome frame = new UserWelcome();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;

	public UserWelcome() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		JOptionPane.showMessageDialog(null, "Connected to Database");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 818, 476);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("New label");
		Image img = new ImageIcon(this.getClass().getResource("welcome.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(-56, 0, 865, 182);
		contentPane.add(lblNewLabel);

		JLabel lblDoYouWant = new JLabel("Do you want to Buy This Items Click and Go...");
		lblDoYouWant.setForeground(Color.RED);
		lblDoYouWant.setFont(new Font("Adobe Caslon Pro Bold", Font.BOLD, 18));
		lblDoYouWant.setBounds(242, 193, 390, 25);
		contentPane.add(lblDoYouWant);

		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Dvd.main(new String[] {});

			}
		});
		Image img1 = new ImageIcon(this.getClass().getResource("DVDD.png")).getImage();
		btnNewButton.setIcon(new ImageIcon(img1));
		btnNewButton.setBounds(197, 229, 192, 153);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("New button");
		Image img2 = new ImageIcon(this.getClass().getResource("books.png")).getImage();
		btnNewButton_1.setIcon(new ImageIcon(img2));
		btnNewButton_1.setBounds(447, 229, 185, 153);
		contentPane.add(btnNewButton_1);

		JLabel lblToBuyDvds = new JLabel("To Buy DVDs..");
		lblToBuyDvds.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblToBuyDvds.setBounds(242, 388, 146, 20);
		contentPane.add(lblToBuyDvds);

		JLabel label = new JLabel("To Buy Books..");
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setBounds(474, 388, 146, 20);
		contentPane.add(label);
	}
}
