import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JLabel;

public class ViewDetails extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewDetails frame = new ViewDetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;
	private JTable table_1;
	private JButton button;
	private JButton btnBackToAdmin;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_5;
	private JLabel label_6;
	private JLabel label_7;
	private JLabel label_8;

	public ViewDetails() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "sanjsi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 834, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		table = new JTable();
		table.setBounds(20, 70, 375, 214);
		contentPane.add(table);

		JButton btnViewBuyDvd = new JButton("View Buy DVD Details");
		btnViewBuyDvd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String sql = ("select * from bookstore.buydvd");
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) table.getModel();
					tm.setRowCount(0);

					table.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnViewBuyDvd.setBounds(115, 304, 159, 23);
		contentPane.add(btnViewBuyDvd);

		table_1 = new JTable();
		table_1.setBounds(437, 70, 371, 214);
		contentPane.add(table_1);

		button = new JButton("View Buy Book Details");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String sql = ("select * from bookstore.buybook");
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) table_1.getModel();
					tm.setRowCount(0);

					table_1.setModel(DbUtils.resultSetToTableModel(rs));

				} catch (Exception e1) {
					System.out.println(e1);
				}
			}
		});
		button.setBounds(551, 295, 159, 23);
		contentPane.add(button);

		btnBackToAdmin = new JButton("Back To Admin Home");
		btnBackToAdmin.setForeground(Color.RED);
		btnBackToAdmin.setFont(new Font("Stencil Std", Font.BOLD, 13));
		btnBackToAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminWelcome.main(new String[] {});
			}
		});
		Image img5 = new ImageIcon(this.getClass().getResource("back.png")).getImage();
		btnBackToAdmin.setIcon(new ImageIcon(img5));
		btnBackToAdmin.setBounds(10, 375, 272, 47);
		contentPane.add(btnBackToAdmin);
		
		JLabel lblDvdnumber = new JLabel("DVDNumber");
		lblDvdnumber.setBounds(20, 45, 82, 14);
		contentPane.add(lblDvdnumber);
		
		JLabel label = new JLabel("DVDName");
		label.setBounds(96, 45, 82, 14);
		contentPane.add(label);
		
		label_1 = new JLabel("UserName");
		label_1.setBounds(170, 45, 70, 14);
		contentPane.add(label_1);
		
		label_2 = new JLabel("Price");
		label_2.setBounds(247, 45, 44, 14);
		contentPane.add(label_2);
		
		label_3 = new JLabel("CreditCardNumber");
		label_3.setBounds(301, 45, 110, 14);
		contentPane.add(label_3);
		
		label_4 = new JLabel("BookNumber");
		label_4.setBounds(437, 45, 82, 14);
		contentPane.add(label_4);
		
		label_5 = new JLabel("BookName");
		label_5.setBounds(521, 45, 82, 14);
		contentPane.add(label_5);
		
		label_6 = new JLabel("UserName");
		label_6.setBounds(596, 45, 70, 14);
		contentPane.add(label_6);
		
		label_7 = new JLabel("CreditCardNumber");
		label_7.setBounds(718, 45, 110, 14);
		contentPane.add(label_7);
		
		label_8 = new JLabel("Price");
		label_8.setBounds(666, 45, 44, 14);
		contentPane.add(label_8);
	}
}
